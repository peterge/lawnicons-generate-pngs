#!/bin/bash

# Input directory containing PNG files
PNG_DIR="pngs"
# Output directory for expanded PNG files
EXPANDED_PNG_DIR="expanded_pngs"
# Amount of expansion (in pixels)
EXPANSION_AMOUNT=36

# Remove the output directory if it exists
rm -rf "$EXPANDED_PNG_DIR"
# Ensure output directory exists
mkdir -p "$EXPANDED_PNG_DIR"

# Iterate over each PNG file in the directory
for PNG_FILE in "$PNG_DIR"/*.png;
do
  # Extract the base name of the file (without directory and extension)
  BASENAME=$(basename "$PNG_FILE" .png)
  # Define the output expanded PNG file path
  EXPANDED_PNG_FILE="$EXPANDED_PNG_DIR/$BASENAME.png"

  # Get the background color of the original PNG file
  BACKGROUND_COLOR=$(convert "$PNG_FILE" -format "%[pixel:p{0,0}]" info:-)

  # Expand the PNG file with the background color
  convert "$PNG_FILE" -bordercolor "$BACKGROUND_COLOR" -border "$EXPANSION_AMOUNT"x"$EXPANSION_AMOUNT" "$EXPANDED_PNG_FILE"

  echo "Expanded $PNG_FILE to $EXPANDED_PNG_FILE."
done

echo "Expansion completed for all PNG files."

