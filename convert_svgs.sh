#!/bin/bash

# Directory containing SVG files
SVG_DIR="svgs"
# Output directory for PNG files
PNG_DIR="pngs"
# Background color
BACKGROUND_COLOR="black"
# Foreground color
FOREGROUND_COLOR="white"

# Remove the output directory if it exists
rm -rf "$PNG_DIR"
# Ensure output directory exists
mkdir -p "$PNG_DIR"

# Count the total number of SVG files
TOTAL_SVGS=$(ls "$SVG_DIR"/*.svg | wc -l)
CURRENT_COUNT=0

# Iterate over each SVG file in the directory
for SVG_FILE in "$SVG_DIR"/*.svg; 
do
  CURRENT_COUNT=$((CURRENT_COUNT + 1))
  # Extract the base name of the file (without directory and extension)
  BASENAME=$(basename "$SVG_FILE" .svg)
  # Define the output PNG file path
  PNG_FILE="$PNG_DIR/$BASENAME.png"

  # Create a temporary SVG file with modified stroke and fill colors
  TEMP_SVG_FILE="temp_$BASENAME.svg"
  
  # Replace stroke colors
  sed -E 's/stroke="[^"]*"/stroke="'$FOREGROUND_COLOR'"/g' "$SVG_FILE" > "$TEMP_SVG_FILE"
  # Replace fill colors to none
  sed -i '' -E 's/fill="[^"]*"/fill="none"/g' "$TEMP_SVG_FILE"

  # Convert the modified SVG to PNG with transparent background using rsvg-convert
  rsvg-convert -w 192 -h 192 "$TEMP_SVG_FILE" -o temp.png

  # Apply the black background using ImageMagick
  convert temp.png -background "$BACKGROUND_COLOR" -flatten "$PNG_FILE"

  # Remove the temporary files
  rm temp.png "$TEMP_SVG_FILE"
  
  echo "$CURRENT_COUNT/$TOTAL_SVGS Converted $SVG_FILE to $PNG_FILE."
done

echo "All $TOTAL_SVGS SVG files have been converted to PNGs."

