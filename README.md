# lawnicons-generate-pngs

These both scripts can be used to generate pngs from lawnicons, to set them as icons in iOS.

1. Clone lawnicons:

`git clone git@github.com:LawnchairLauncher/lawnicons.git`

2. Copy both scripts to `lawnicons/`:

`cp lawnicons-generate-pngs/convert_svgs.sh	lawnicons-generate-pngs/expand_png.sh lawnicons/`

3. Run both scripts:

`bash convert_svgs.sh` and `bash expand_png.sh`

4. You should receive the png files in `lawnicons/expanded_pngs/`.

Other options:
- You can set the color of the png files in the `BACKGROUND_COLOR` and `FOREGROUND_COLOR` variable in `convert_svgs.sh`.
- You can set the pixel count by which the pngs get expanded in the `EXPANSION_AMOUNT` variable in `expand_png.sh`.

# Custom icons

I will keep some icons in the `custom-icons-not-accepted-by-upstream` folder, that are [not accepted by lawnchair](https://github.com/LawnchairLauncher/lawnicons/pull/2105) or that are no android icons (like safari), or that needed some modifications, because the svg conversation did not work well (like teams).

# copyright

These scripts have been written with the help of GPT 4o.